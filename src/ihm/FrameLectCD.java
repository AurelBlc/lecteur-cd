package ihm;

import javax.swing.*;
import javax.swing.plaf.PanelUI;

import java.awt.*;
import datas.*;
import control.*;

public class FrameLectCD extends JFrame{

	// Tous les widgets en attributs
	private JButton bPlay = new JButton("PLAY");
	private JButton bStop = new JButton("STOP");
	private JButton bNext = new JButton("NEXT");
	private JButton bPrevious = new JButton("PREVIOUS");
	private JButton bChargerCD = new JButton("Charger CD");
	
	private JTextField tempsTot = new JTextField("");
	private JTextField nbPlages = new JTextField("");
	private JTextField plageCourante = new JTextField("");
	private JTextField infoMorceau = new JTextField("");
	private JTextField dureePlageCourante = new JTextField("");

	private JLabel jImage;
	private ImageIcon image;

	private LecteurCD theLecteur;
	
	public FrameLectCD (String titre){
		super(titre);
		theLecteur = new LecteurCD();
		miseEnPlaceDecor();
		attacherEcouteurs();
		setSize(800,400);
		setTitle(titre);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void miseEnPlaceDecor(){
		//découpage graphique
		this.setLayout(new GridLayout(1,2));
		this.add(partieGauche());
		this.add(partieDroite());
		
	}
	private void attacherEcouteurs(){
		//écouter d'actions de souris
		this.bPlay.addActionListener(new Ecouteur(this));
		this.bStop.addActionListener(new Ecouteur(this));
		this.bNext.addActionListener(new Ecouteur(this));
		this.bPrevious.addActionListener(new Ecouteur(this));
		this.bChargerCD.addActionListener(new Ecouteur(this));
	}

	private JPanel partieGauche(){

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3,2));
		panel.add(partieHaut());
		panel.add(partieCentre());
		panel.add(partieBas());

		return panel;
	}

	private JPanel partieDroite(){
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1,1));
		image = new ImageIcon("walkman.png");
		ImageIcon icon = new ImageIcon(image.getImage().getScaledInstance(400, 400, Image.SCALE_DEFAULT));
		jImage = new JLabel(icon);
		panel.add(jImage);

		return panel;
	}

	private JPanel partieHaut(){

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3,2));
		panel.add(bChargerCD);
		panel.add(new JLabel());
		panel.add(new JLabel("Temps total"));
		panel.add(tempsTot);
		panel.add(new JLabel("Nombre de plages"));
		panel.add(nbPlages);

		return panel;
	}
	private JPanel partieCentre(){

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());		
		panel.add(plageCourante, BorderLayout.WEST);
		panel.add(infoMorceau, BorderLayout.CENTER);
		panel.add(dureePlageCourante, BorderLayout.EAST);

		/* Modification de la taille des fenêtres*/
		plageCourante.setPreferredSize(new Dimension(50,100));
		dureePlageCourante.setPreferredSize(new Dimension(100,100));
		
		/* Modification de la couleur de fond des fenêtres*/
		plageCourante.setBackground(Color.YELLOW);		
		dureePlageCourante.setBackground(Color.YELLOW);
		infoMorceau.setBackground(Color.YELLOW);

		/* Texte des fenêtres centré*/
		plageCourante.setHorizontalAlignment(JTextField.CENTER);
		dureePlageCourante.setHorizontalAlignment(JTextField.CENTER);
		infoMorceau.setHorizontalAlignment(JTextField.CENTER);
	
		return panel;
	}

	private JPanel partieBas(){

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1,4));
		panel.add(bStop);
		panel.add(bPlay);
		panel.add(bNext);
		panel.add(bPrevious);

		return panel;
	}

	/* Accesseurs des attributs de classe */

	public LecteurCD getTheLecteur(){
		return theLecteur;
	}

	public JButton getBPlay(){
		return bPlay;
	}
	
	public JButton getBStop(){
		return bStop;
	}
	
	public JButton getBNext(){
		return bNext;
	}
	
	public JButton getBPrevious(){
		return bPrevious;
	}
	
	public JButton getBChargerCD(){
		return bChargerCD;
	}

	/* Modificateurs des attirbuts de classe*/

	public void setButton(){
		if(theLecteur.estCharge()){
			bChargerCD.setText("Retirer CD");
		}
		else{
			bChargerCD.setText("Charger CD");
		}
	}

	public void setTempsTot(String temps){
		tempsTot.setText(temps);
	}
	
	public void setNbPlages(String nbPlage){
		nbPlages.setText(nbPlage);
	}
	
	public void setPlageCourante(String plage){
		plageCourante.setText(plage);
	}
	
	public void setInfoMorceau(String info){
		infoMorceau.setText(info);
	}
	
	public void setDureePlageCourante(String infoDuree){
		dureePlageCourante.setText(infoDuree);
	}

	public void setImage(String fichier){
		if(theLecteur.estCharge()){
			ImageIcon pict = new ImageIcon(fichier);
			ImageIcon imageResized = new ImageIcon(pict.getImage().getScaledInstance(400, 400, Image.SCALE_DEFAULT));
			jImage.setIcon(imageResized);
		}
		else{
			image = new ImageIcon("walkman.png");
			ImageIcon icon = new ImageIcon(image.getImage().getScaledInstance(400, 400, Image.SCALE_DEFAULT));
			jImage.setIcon(icon);
		}
	}
	
	public static void main (String [] args){
		FrameLectCD appli = new FrameLectCD("Lecteur CD");
	}

	
}
