package control;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JTextField;

import ihm.*;
import datas.*;

import java.io.File;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFileChooser;

public class Ecouteur implements ActionListener{
	
	private FrameLectCD lect;
	
	public Ecouteur (FrameLectCD theL){

		this.lect = theL;
	}
	
	public void actionPerformed (ActionEvent e){
		
		if (e.getSource() == lect.getBPlay()){	

			if(lect.getTheLecteur().estCharge() && lect.getTheLecteur().getCD()!=null){
				lect.getTheLecteur().play();
				int numPlage = lect.getTheLecteur().getIndexCourant();
				/*Affichage piste de lecture => WEST/CENTER/EAST*/
				/*WEST = n° de la piste courante*/
				lect.setPlageCourante(String.valueOf(numPlage));

				//System.out.println(String.valueOf(numPlage));

				/*CENTER = info de la piste courante (Titre-Interprète-durée)*/
				String info = lect.getTheLecteur().getCD().getUnePlage(numPlage).toString();
				lect.setInfoMorceau(info);

				/*EAST = durée de la plage*/
				String duree = lect.getTheLecteur().getCD().getUnePlage(numPlage).getLaDuree().enTexte('H');
				lect.setDureePlageCourante(duree);

			}
		}
		
		if (e.getSource() == lect.getBStop()){
			
			if(lect.getTheLecteur().estCharge()){
				//System.out.println("=========================J'ai appuyé sur STOP");
				
				/*Affichage du n° de la piste => n'affiche plus rien quand on retire le CD*/
				lect.setPlageCourante(null);

				/*Affichage des infos du morceau => n'affiche plus rien quand on retire le CD*/
				lect.setInfoMorceau(null);

				/*Affichage du nombre de plages => n'affiche plus rien quand on retire le CD*/
				lect.setDureePlageCourante(null);
			}
		}
		
		if (e.getSource() == lect.getBNext()){

			if(lect.getTheLecteur().estCharge()){
				//System.out.println("=========================J'ai appuyé sur NEXT");

				int nbPlageCD = lect.getTheLecteur().getCD().getNbrePlages();
				
				/*Affichage avec le nouvel index*/
			
				/* On incrémente de 1 l'index */
				lect.getTheLecteur().next();
				int numPlage = lect.getTheLecteur().getIndexCourant();
				System.out.println("===après play()===");
				System.out.println(String.valueOf(numPlage));
				
						
				/*WEST = n° de la piste courante*/
				lect.setPlageCourante(String.valueOf(numPlage));

				/*CENTER = info de la piste courante (Titre-Interprète-durée)*/
				String info = lect.getTheLecteur().getCD().getUnePlage(numPlage).toString();
				lect.setInfoMorceau(info);

				/*EAST = durée de la plage*/
				String duree = lect.getTheLecteur().getCD().getUnePlage(numPlage).getLaDuree().enTexte('H');
				lect.setDureePlageCourante(duree);
				
			}
		}
		
		if (e.getSource() == lect.getBPrevious()){
			
			if(lect.getTheLecteur().estCharge()){
				//System.out.println("=========================J'ai appuyé sur PREVIOUS");

				/*Affichage avec le nouvel index*/
				
				/* On décrémente de 1 l'index */
				lect.getTheLecteur().previous();
				int numPlage = lect.getTheLecteur().getIndexCourant();
			
				/*WEST = n° de la piste courante*/
				lect.setPlageCourante(String.valueOf(numPlage));

				System.out.println(String.valueOf(numPlage));

				/*CENTER = info de la piste courante (Titre-Interprète-durée)*/
				String info = lect.getTheLecteur().getCD().getUnePlage(numPlage).toString();
				lect.setInfoMorceau(info);

				/*EAST = durée de la plage*/
				String duree = lect.getTheLecteur().getCD().getUnePlage(numPlage).getLaDuree().enTexte('H');
				lect.setDureePlageCourante(duree);
			
				
			}
		}
		
		if (e.getSource() == lect.getBChargerCD()){			
			if(!lect.getTheLecteur().estCharge()){
				/*Récupération du dossier CD*/
				JFileChooser dialogue = new JFileChooser(new File("."));
				PrintWriter sortie;
				File file;
				String[] mesLigneAcces;
				String[] monFichier;
				
				if (dialogue.showOpenDialog(null)== JFileChooser.APPROVE_OPTION) {
					file = dialogue.getSelectedFile();
					System.out.println(file);
					
					String leFich = file.toString();

					/*Le lecteur est vide, un CD est chargé => passage de estCharge() à true*/
					lect.getTheLecteur().chargerUnCD(leFich);

					/* Modification de l'image*/
					String image = "" + lect.getTheLecteur().getCD().getLInterpreteCD() +".png";
					lect.setImage(image);
				}
				
				
				/*Modification du label du bouton "ON/OFF"*/
				lect.setButton();
				

				/*Modifications des affichages des différents champs*/
				/*Affichage temps total*/
				Duree dureeTotCD = lect.getTheLecteur().getCD().getDureeTotale();
				lect.setTempsTot(dureeTotCD.enTexte('H'));

				/*Affichage Nombre de plages*/
				int nbPlageCD = lect.getTheLecteur().getCD().getNbrePlages();
				lect.setNbPlages(String.valueOf(nbPlageCD));
			}
			
			else {
				//System.out.println("==================Je suis rentré dans la boucle du bouton OFF");
				/*Un CD est chargé, il est retiré du lecteur => passage de estCharge() à false*/
				lect.getTheLecteur().setDecharger();
				
				/* Modification de l'image*/
				lect.setImage("");

				/*Modification du label du bouton "ON/OFF"*/
				lect.setButton();

				/*Modifications des affichages des différents champs*/
				/*Affichage temps total => n'affiche plus rien quand on retire le CD*/
				lect.setTempsTot(null);

				/*Affichage Nombre de plages => n'affiche plus rien quand on retire le CD*/
				lect.setNbPlages(null);

				/*Affichage du n° de la piste => n'affiche plus rien quand on retire le CD*/
				lect.setPlageCourante(null);

				/*Affichage des infos du morceau => n'affiche plus rien quand on retire le CD*/
				lect.setInfoMorceau(null);

				/*Affichage du nombre de plages => n'affiche plus rien quand on retire le CD*/
				lect.setDureePlageCourante(null);
			}
		}
		
	}
	
	
}
