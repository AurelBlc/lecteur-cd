package datas;

/**
* Cette classe définit une durée temporelle. Elle permet la manipulation d'intervalles de temps.
* Une durée s'exprime en millisecondes.
* @author Aurélien
*/

public class Duree {

	/**
	* La durée s'exprime en milliseconde
	*/
	private long leTemps;

	/**
	* Constructeur avec initialisation en millisecondes
	* @param millisec la durée exprimée en millisecondes
	*/
	public Duree(long millisec){
		if (millisec<0){
			System.out.println("Mauvais format de durée !");
		}
		else{
				this.leTemps = millisec;
		}
	}

	/**
	* Constructeur à partir des données heures, minutes, secondes.
	* @param heures nbres d'heures
	* @param minutes nbres de minutes
	* @param secondes nbres de secondes
	*/
	public Duree(int heures, int minutes, int secondes){
		if(heures>0 && minutes>0 && secondes>0){
			if (minutes < 60 && secondes < 60){
				this.leTemps = (heures*3600 + minutes*60 + secondes)*1000;
				System.out.println("là");
			}
			else {
				System.out.println("Mauvais format de durée !");
			}
		}

		else {
			System.out.println("Mauvais format de durée !");
		}
	}

	/**
	* Construction par recopie d'une Duree passée en paramètre.
	* @param autreDuree la durée à copier
	*/
	public Duree (Duree autreDuree){
		this.leTemps = autreDuree.getLeTemps();
	}

	/**
	* Accesseur qui retourne la valeur de la durée en milliseconde
	*/
	public long getLeTemps(){
		return this.leTemps;
	}

	/**
	* Modificateur qui ajoute une durée à la durée courante
	* @param autreDuree durée à rajouter
	*/
	public void ajoute(Duree autreDuree){
		this.leTemps = this.leTemps + autreDuree.getLeTemps();
	}

	/**
	* Accesseur qui effectue une comparaison entre la durée coura,te et une autre durée.
	* @param autreDuree durée à comparer à la durée courante
	* @return
	* un entier qui prend les valeurs suivantes :
	* <ul><li> -1 : si la durée courante est + petite que autreDuree
	* <li> 0 : qi la durée courante est égale à autreDuree
	* <li> 1 : si la durée courante est + grande que autreDuree</ul>
	*/

	public int compareA(Duree autreDuree){


		int rep;

		if (this.leTemps != autreDuree.getLeTemps()){
			if (this.leTemps < autreDuree.getLeTemps()){
				rep = -1;
			}
			else{
				rep = 1;
			}
		}
		else{
			rep = 0;
		}

		return rep;
	}

	/**
	* Accesseur qui renvoie sous la forme d'une chaine de caractères la durée
	* courante
	* @param mode - décide la forme donnée à la chaîne de caractères
	* La forme de la chaîne de caractères dépend du "mode" (caractère passé en paramètre) choisi
	* <ul><li> si mode == 'J' => chaîne de caractères de la forme "JJJ jours HH h"
	* <li> si mode == 'H' => chaîne de caractères de la forme "HHH:MM:SS"
	* <li> si mode == 'S' => chaîne de caractères de la forme "SSS.MMM sec"
	* <li> si mode == 'M' => chaîne de caractères de la forme "MMMMMM millisec"</ul>
	* @return la durée sous la forme d'une chaine de caractères
	* <br>
	* La méthode utilise la méthode privée neJHMS() pour extraire dans un tableau d'entriers
	* séparément le nombre de jours, le nombre d'heures, le nombre de minutes, le nombre de
	* secondes et le nombre de millisecondes que contient la durée courante (leTemps).
	*/
	public String enTexte(char mode){

		String texte = "";
		int temps[] = enJHMS();
		int jour = temps[0];
		int heure = temps[1];
		int heureTot = (jour*24) + heure;
		int minute = temps[2];
		int sec = temps[3];
		int secTot =  (jour*24*60*60) + (heure*60*60) + (minute*60) + sec ;
		int millisec = temps[4];
		int millisecTot = (jour*24*60*60*1000) + (heure*60*60*1000) + (minute*60*1000) + (sec*1000) + millisec;

		switch(mode){
			case 'J' : texte = texte + jour + " jour(s) " + heure + " h" ;	break;
			case 'H' : texte = texte + heureTot + ":" + minute + ":" + sec; break;
			case 'S' : texte = texte + secTot + "." + millisec + " sec" ; break;
			case 'M' : texte = texte + millisecTot + " millisec" ; break;
		}


		return texte;
	}

	/**
	* Méthode privée qui effectue un découpage de la durée en intervalles
	* (jours, heures, minutes, secondes, millisecondes)
	* La durée courante (leTemps) est analysée pour fabriquer un
	* tableau d'entiers (taille 5) dont chaque élément a la signature suivante:
	* <ul><li> ret[0] contient le nombre de jours
	* <li> ret[1] contient le nombre de d'heures (<24h)
	* <li> ret[2] contient le nombre de minutes
	* <li> ret[3] contient le nombre de secondes
	* <li> ret[4] contient le nombre de millisecondes</ul>
	* @return un tableau d'entiers
	*/
	private int[] enJHMS(){

		int temps[]= new int[5];

		int jours;
		int heures;
		int minutes;
		int sec;
		int millisec;
		int reste;

		jours = (int)this.leTemps / 86400000; //1j = 86400000 millisecondes
		reste = (int)(this.leTemps % 86400000);
		heures = reste / 3600000; //1h = 3600000 millsecondes
		reste = reste % 3600000;
		minutes = reste / 60000; //1 minute = 60000 millisec
		reste = reste % 60000;
		sec = reste / 1000; // 1sec = 1000 millisec
		reste = reste % 1000;
		millisec = reste;

		temps[0] = jours;
		temps[1] = heures;
		temps[2] = minutes;
		temps[3] = sec;
		temps[4] = millisec;

		return temps;
	}
}
