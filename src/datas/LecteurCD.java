package datas;

/**
* La classe LecteurCD simule de manière simplifiée les fonctionnalités d'un lecteur de CD, à savoir :
*<ul><li>Le chargement d'un CD dans le LecteurCD
* <li>La lecture du CD depuis le début (touche PLAY)
* <li>L'arrêt de la lecture (touche STOP)
* <li>Le passage au morceau suivant (touche NEXT)
* <li>Le passage au morceau précédent (touche PREVIOUS)</ul>
* @author Aurélien
*/

public class LecteurCD {

  /**
  * Le lecteur renferme-t-il un CD?
  */
  private boolean estCharge;

  /**
  * L'index de la plage courante (1 <= index <= nbrePlages ou zéro si aucun CD chargé)
  */
  private int indexPlage;

  /**
  * Le CD courant (qui se trouve dans le lecteur)
  */
  private CD leCDCourant;

  /**
  * Construction d'un lecteur CD. A l'issue de la construction, il n'y a aucun CD dans le LecteurCD
  * (leCDCourant=null). Le chargement d'un CD se fait à l'aide de la méthode "chargerUnCD()".
  */
  public LecteurCD(){
    this.estCharge = false;
    this.indexPlage = 0;
    this.leCDCourant= null;
  }

  /**
  * Accesseur qui renvoie le temps total du CD chargé sous forme d'une chaine de caractères.
  * @return le temps total du CD
  */
  public String getTempsTotal(){
    String dureeTot = "";
    Duree duree = leCDCourant.getDureeTotale();
    dureeTot = duree.enTexte('H');

    return dureeTot;
  }

  /**
  * Accesseur qui renvoie le nombre de plages que contient un CD.
  * @return le nombre de plages (-1 si aucun CD chargé)
  */
  public int getNbrePlages(){
    int nbPlage;
    if(estCharge=false){
      nbPlage = -1;
    }
    else{
      nbPlage = leCDCourant.getNbrePlages();
    }
    return nbPlage;
  }

  /**
  * Accesseur qui renvoie l'index de la plage du CD en cours de lecture. Cet index est compris
  * entre 1 et nbrPlages.
  * @return l'index de la plage courante (zéro si aucun CD chargé)
  */
  public int getIndexCourant(){
    return indexPlage;
  }

  /**
  * Accesseur qui renvoie la plage en cours de lecture.
  * @return la plage courante (null si aucun CD chargé)
  */
  public Plage getPlageCourante(){
    Plage plage;
    if(!estCharge){
      plage = null;
    }
    else{
      plage = leCDCourant.getUnePlage(getIndexCourant());
    }
    return plage;
  }

  /**
  * Accesseur qui renvoie vrai si le lecteur contient un CD
  * @return vrai si il y a un CD dans le lecteur
  */
  public boolean estCharge(){
    boolean rep = false;
    if(estCharge){
		  rep = true;
	  }    
    return rep;
  }
  
   /**
  * Accesseur qui renvoie le CD chargé dans le lecteur ou null si aucun CD chargé
  * @return le CD courant ou null si aucun Cd dans le lecteur
  */
  public CD getCD(){
    CD cd; 
    if (estCharge){
		cd = leCDCourant;
    }
    else {
      cd = null;
    }
    return cd;    
    }

  /**
  * Modificateur qui force le lecteur à se décharger du CD qu'il contient. Le booléen estCharge
  * devient faux et leCDCourant devient null
  */
  public void setDecharger(){
    estCharge=false;
    leCDCourant=null;
  }

  /**
  * Modificateur dont le rôle est de charger un CD dans le lecteur. Pour simplifier, il s'agira
  * toujours du même CD pour cette version de la méthode. La méthode doit construire le CD et
  * mettre le booléen à vrai.
  */
  public void chargerUnCD(){
	leCDCourant = new CD("un interprete","un titre");
    this.estCharge = true;
  }

  /**
  * Modificateur dont le rôle est de charger un CD dans le lecteur. Pour cette version, le CD est
  * construit à partir d'un fichier texte.
  */
  public void chargerUnCD(String leFich){
    leCDCourant = new CD(leFich);
    this.estCharge = true;
  }
  
   /**
   * Modificateur qui simule la touche STOP. Cela a pour conséquence de simplement remettre
   * l'index des plage sur 1. (Et éventuellement d'arrêter le timer de lecture de la plage en cours)
   * Il ne se passe rien si aucun CD chargé
   */
   public void stop(){
     if(estCharge){
       this.indexPlage=1;
     }
   }

   /**
   * Modificateur qui simule la touche PLAY. Cela a pour conséquence de simplement remettre
   * l'index des plages sur 1.(Et éventuellement de démarrer le timer de lecture de la 1ère plage)
   * Il ne se passe rien si aucun CD chargé
   */
   public void play(){
     if(estCharge){
       this.indexPlage=1;
     }
   }

   /**
   * Modificateur qui simule la touche NEXT. Cela a pour conséquence de simplement
   * incrémenter l'index des plages.(Et éventuellement de démarrer le timer de lecture de la plage
   * suivante)
   * Il ne se passe rien si aucun CD chargé.
   */
   public void next(){
     if((estCharge) && (getIndexCourant()<leCDCourant.getNbrePlages())){
       this.indexPlage = indexPlage + 1;
     }
   }

   /**
   * Modificateur qui simule la touche PREVIOUS. Cela a pour conséquence de simplement
   * décrémenter l'index des plages.(Et éventuellement de démarrer le timer de lecture de la plage
   * suivante)
   * Il ne se passe rien si aucun CD chargé.
   */
   public void previous(){
     if((estCharge) && getIndexCourant()>1){
       this.indexPlage = indexPlage - 1;
     }
   }

}
