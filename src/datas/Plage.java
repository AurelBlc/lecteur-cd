package datas;

/**
* Cette classe définit une plage de musique appartenant à un CD. UNe plage a une durée et peut se jouer.
* Elle est caractérisée par un titre (titre du morceau) et un(des) interprète(s) (chanteur, musiciens, ...)
* du morceau de musique.
* @author Aurélien
*/

public class Plage{
	/**
	* Durée du morceau
	*/
	private Duree laDuree;

	/**
	* Titre du morceau de musique
	*/
	private String leTitre;
	
	/**
	* Interprète(s) du morceau de musique
	*/
	private String lInterprete;

	/**
	 * Construction d'une plage correspondant à un morceau de musique
	 * @param duree durée de la plage(!!type Duree)
	 * @param titre titre deu morcecau de musique
	 * @param interprete l'interprète (les interprètes) du morceau
	 */
	public Plage (Duree duree, String titre, String interprete){
		this.laDuree = duree;
		this.leTitre = titre;
		this.lInterprete = interprete;
	}

	/**
	 * Accesseur qui renvoie la durée
	 * @return la durée (!!type Duree)
	 */
	 public Duree getLaDuree(){
		 return laDuree;
	 }

	/**
	* Accesseur qui renvoie le titre du morceau
	* @return le titre
	*/
	public String getLeTitre(){
	  return this.leTitre;
	}

	/**
	*Accesseur qui renvoie l'interprète (les interprètes) du morceau
	* @return l'interprète
	*/

	public String getLInterprete(){
	  return this.lInterprete;
	}

	/**
	* Accesseur qui renvoie un texte décrivant complètement la plage sous
	* la forme suivante :
	* <ul><li>Description de la plage de musique
	* <li>Titre : ...
	* <li>interprète : ...
	* <li>Durée : HHH:MM:SS</ul>
	* @return le texte qui décrit la plage
	*/
	public String getFicheComplete(){

		String ficheComplete = "Description de la plage de musique\n"
							+ "Titre : " + this.leTitre + "\n"
							+ "Interprete : " + this.lInterprete + "\n"
							+ "Duree : " + this.laDuree.enTexte('S');

	  return ficheComplete;
	}

	/**
	* Accesseur qui renvoie un résumé textuel de la plage sous la forme :
	* titre-interprète-durée(SSS.MMM sec)
	* @return la ligne de texte qui décrit la plage
	*/
	@Override
	public String toString(){
	  return getLeTitre() + " - " + getLInterprete() + " - " + laDuree.enTexte('S');
	}
}
