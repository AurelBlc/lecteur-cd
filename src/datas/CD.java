package datas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
* Un CE est un ensemble de plages musicales. Le CD est caractérisé par un titre, un interprète (ou
* des interprètes s'il s'agit d'un groupe). Lors de sa création, le CD est vierge. En le gravant, il sera rempli
* progressivement de morveau (plages).
* @author Aurélien
*/

public class CD {
	/**
	* Le tableau contenant toutes les plages du CD
	*/
	private ArrayList<Plage> lesPlages = new ArrayList<Plage>();

	/**
	* Le titre du CD
	*/
	private String leTitreCD;

	/**
	* interprète(s) du CD
	*/
	private String lInterpreteCD;

	/**
	* Construction d'un CD.
	* Cette construction se fait en 2 temps :
	* <ol><li>d'abord il y a création d'un CD vierge
	* <li>ensuite il sera gravé (ajout de plage) par appel de la méthode privée "graverCD()"
	* @param interpreteCD le(les) interprètes du CD
	* @param titreCD le titre du CD
	*/
	public CD(String interpreteCD, String titreCD){
		this.lInterpreteCD = interpreteCD;
		this.leTitreCD = titreCD;
		this.graverCD();
	}
	/**
	* Construction d'un CD.
	* Cette construction se fait à partir d'un ficher texte qui contient toutes les informations :
	* <ul><li>le titre et l'interprète du CD
	* <li> les différentes plages au format TitrePlage/ InterprètePlage/ X min./ Y sec.</ul>
	* La méthode privée "graverCD (String leFich)" effectue la lecture du fichier texte.
	* @param leFich le nom du fichier texte à lire
	*/
	public CD(String leFich){
		graverCD(leFich);
	}

	/**
	* Accesseur qui renvoie le nombre de plages gravées sur le CD.
	* @return le nombre total de plages
	*/
	public int getNbrePlages(){
		int plages = lesPlages.size();
		return plages;
	}

	/**
	 * Accesseur qui renvoie le titre du CD
	 * @return le titre du CD
	 */
	public String getLeTitre(){
		return leTitreCD;
	}

	/**
	* Accesseur qui renvoie le(les) interprètes du CD.
	* @return le(les) interprètes
	*/
	public String getLInterpreteCD(){
		return lInterpreteCD;
	}
	/**
	* Accesseur qui calcule et renvoie la durée totale du CD.
	* @return la durée totale
	*/
	public Duree getDureeTotale(){
		Duree laDureeTot = new Duree(0);
		for(Plage plage : lesPlages){
			laDureeTot.ajoute(plage.getLaDuree());
		}
		return laDureeTot;
	}

	/**
	* Accesseur qui renvoie la plage n° index du CD.
	* !! La première plage du CD est à l'index = 1.
	* @param index le n° de la plage à renvoyer
	* @return la plage qui se trouve à l'emplacement (index-1) dans le tableau des plages
	*/
	public Plage getUnePlage(int index){
		return lesPlages.get(index-1);
	}

	/**
	* Méthode privée qui grave le CD à partir d'un fichier. On donne un titre et un interprète au CD de
	* même qu'une liste de plages. Les informations sont lues à partir d'un fichier texte.
	* @param leFich le nom du fichier texte à lire
	*/
	private void graverCD(String leFich){
		String[] mesLignes;
		String tmp;
		BufferedReader tampon;

		try{
			tampon = new BufferedReader(new FileReader(leFich));		
			tmp = tampon.readLine();
			mesLignes = tmp.split("/");
			if(tampon.readLine() != null) {
				leTitreCD = mesLignes[0];
				lInterpreteCD = mesLignes[1];
			}
			String ligne;

			while((ligne = tampon.readLine()) != null){

				mesLignes = ligne.split("/");
				int min =  Integer.parseInt(mesLignes[2]);
				int sec = Integer.parseInt(mesLignes[3]);
				Duree minutes = new Duree (min*60*1000);
				Duree secondes = new Duree(sec*1000);

				minutes.ajoute(secondes);

				String titre = mesLignes[0];
				String interprete = mesLignes[1];

				Plage plage = new Plage(minutes, titre, interprete);
				lesPlages.add(plage);
				
			}

			tampon.close();
		}
		catch(FileNotFoundException e){
			System.out.println(e.getMessage());
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
		
	}


	
	/**
	 * Méthode privée qui grave le CD. Un certain nombre de plages sont ajoutées au CD. Dans cette
	 * version simplifiée, les plages sont créées et ajoutées " à la main" au CD
	 */
	private void graverCD(){
		Plage plage1 = new Plage(new Duree(180000),"TNT","AC~DC");
		Plage plage2 = new Plage(new Duree(360000),"Don't Speak","No Doubt");
		Plage plage3 = new Plage(new Duree(350500),"Stairway to Heaven","LedZep");
		Plage plage4 = new Plage(new Duree(420000),"Bohemian Rapsody","Queen");
		
		lesPlages.add(plage1);
		lesPlages.add(plage2);
		lesPlages.add(plage3);
		lesPlages.add(plage4);
	}
}
