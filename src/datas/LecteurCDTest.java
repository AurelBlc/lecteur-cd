package datas;


class LecteurCDTest{
    public static void main (String[] args){
        //Test du constructeur LecteurCD()
        System.out.println("====Test du constructeur LecteurCD()====");
        testConstruct();
        
        //Test du modificateur play()
        System.out.println("====Test du modificateur play()====");
        testPlay();

        //Test du modificateur next()
        System.out.println("====Test du modificateur next()====");
        testNext();

        //Test du modificateur previous()
        System.out.println("====Test du modificateur previous()====");
        testPrev();
        
        //Test du modificateur stop()
        System.out.println("====Test du modificateur stop()====");
        testStop();

    }

    public static void testConstruct(){
        System.out.println("Test cas normal");
        LecteurCD lect = new LecteurCD();
        
        if ((lect.estCharge()==false) && (lect.getIndexCourant()==0) && (lect.getCD()==null)){
            System.out.println("Test OK");
        }
        else{
            System.out.println("Erreur ! Echec du test !");
        }
    }

    public static void testPlay(){
        System.out.println("Test cas normal");
        LecteurCD lect = new LecteurCD();
        lect.chargerUnCD();
        lect.play();

        if ((lect.estCharge()==true) && (lect.getIndexCourant()==1)){
            System.out.println("Test OK");
        }
        else{
            System.out.println("Erreur ! Echec du test !");
        }
    }

    public static void testNext(){
        System.out.println("Test cas normal");
        LecteurCD lect = new LecteurCD();
        lect.chargerUnCD();
        lect.play();
        System.out.println(lect.getIndexCourant());
        lect.next();
        System.out.println(lect.getIndexCourant());
        System.out.println(lect.estCharge());

        if ((lect.estCharge()==true) && (lect.getIndexCourant()==2)){
            System.out.println("Test OK");
        }
        else{
            System.out.println("Erreur ! Echec du test !");
        }
    }

    public static void testPrev(){
        System.out.println("Test cas normal");
        LecteurCD lect = new LecteurCD();
        lect.chargerUnCD();
        lect.play();
        lect.next();
        lect.previous();

        String interprete = "un interprete";
        String titre = "un titre";  

        if ((lect.estCharge()==true) && (lect.getIndexCourant()==1) 
			&& (interprete==lect.getCD().getLInterpreteCD())
            && (titre==lect.getCD().getLeTitre())){
            System.out.println("Test OK");
        }
        else{
            System.out.println("Erreur ! Echec du test !");
        }
    }

    public static void testStop(){
        System.out.println("Test cas normal");
        LecteurCD lect = new LecteurCD();
        lect.chargerUnCD();
        lect.play();
        lect.next();
        lect.stop();        

        boolean estCharge = true;
        int index = 1;

        if ((lect.estCharge()==estCharge) && (lect.getIndexCourant()==1)){
            System.out.println("Test OK");
        }
        else{
            System.out.println("Erreur ! Echec du test !");
        }
    }
}
