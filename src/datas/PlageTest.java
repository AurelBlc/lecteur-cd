package datas;

class PlageTest{

	public static void main (String[] args){

		//Test du constructeur (Plage (Duree duree, String titre, String interprete))
		System.out.println("===Test du contructeur : Plage (Duree duree, String titre, String interprete)===");
		testContruct1();
		

		//Test du toString
		System.out.println("===Test du toString)===");
		testToString();

		
	}

	private static void testContruct1(){
		System.out.println("Test cas normal du constructeur");
		Duree duree = new Duree(250);
		Plage plage = new Plage(duree, "TNT","AC~DC");

		if((plage.getLaDuree().getLeTemps()==250) && (plage.getLeTitre()=="TNT")
			&& (plage.getLInterprete()=="AC~DC")){
			System.out.println("Test OK");
		}

		else{
			System.out.println("Erreur ! Echec du test !");
		}
	}

	private static void testToString(){
		System.out.println("Test cas normal de la méthode ToString");
		Duree duree = new Duree(180000);
		Plage plage = new Plage(duree, "TNT","AC~DC");
		String info = plage.toString();
				
		if(info.equals("TNT - AC~DC - 180.0 sec")){
			System.out.println("Test OK");
		}
		else{
			System.out.println("Erreur ! Echec du test !");
		}
	}

}
