package datas;

class DureeTest{

	public static void main (String[] args){
		//Test du constructeur (long millisec)
		System.out.println("====Test du constructeur (long millisec)====");
		testConstruct1();
		
		//Test du constructeur (int H/M/S)
		System.out.println("====Test du constructeur (int H/M/S)====");
		testConstruct2();
		
		//Test du constructeur (Duree autreDuree)
		System.out.println("====Test du constructeur (Duree autreDuree)====");
		testConstruct3();
		
		//Test des méthodes		
		System.out.println("====Test de la méthode compareA()====");
		testCompareA();
		
		System.out.println("====Test de la méthode ajoute()====");
		testAjoute();
		
		/*System.out.println("====Test de la méthode enJHMS()====");
		testEnJHMS();*/
		
		System.out.println("====Test de la méthode enTexte()====");
		testEnTexte();
		
		
		
		
	}


	private static void testConstruct1(){
		System.out.println("Test cas normal, millisec > 0");
		Duree d1 = new Duree (2000);
		long tps1 = d1.getLeTemps();
		
		if (tps1 == 2000){
			System.out.println("Test OK");
		}
		else{
			System.out.println("Erreur ! Echec du test !");
		}
		
		System.out.println("Test cas d'erreur, millisec < 0, OK si message d'erreur");
		Duree d2 = new Duree (-2000);
		
		System.out.println("Test cas limite, millisec = 0");
		Duree d3 = new Duree (0);
		long tps3 = d3.getLeTemps();
		
		if (tps3 == 0){
			System.out.println("Test OK");
		}
		else{
			System.out.println("Erreur ! Echec du test !");
		}
	}
	
	private static void testConstruct2(){
		System.out.println("Test cas normal, int > 0");
		Duree d1 = new Duree(1,5,54);
		long tps1 = d1.getLeTemps();
		long tpsCalc = ((1*3600)+(5*60)+54)*1000;
		
		if (tpsCalc == tps1){
			System.out.println("Test OK");
		}
		else{
			System.out.println("Erreur ! Echec du test !");
		}
		
		System.out.println("Test cas d'erreur, heure < 0, OK si message d'erreur");
		Duree d2 = new Duree(-1,5,54);
		System.out.println("Test cas d'erreur, minutes < 0, OK si message d'erreur");
		Duree d3 = new Duree(1,-5,54);
		System.out.println("Test cas d'erreur, secondes < 0, OK si message d'erreur");
		Duree d4 = new Duree(1,5,-54);
		System.out.println("Test cas d'erreur, minutes > 59, OK si message d'erreur");
		Duree d5 = new Duree(1,62,54);
		System.out.println("Test cas d'erreur, secondes > 59, OK si message d'erreur");
		Duree d6 = new Duree(1,5,125);
		
	}
	
	private static void testConstruct3(){
		System.out.println("Test cas normal, millisec > 0");
		Duree d1 = new Duree(1500);
		Duree d2 = new Duree(d1);
		
		if(d1.getLeTemps() == d2.getLeTemps()){
			System.out.println("Test OK");
		}
		else{
			System.out.println("Erreur ! Echec du test !");		
		}
		
		System.out.println("Test cas d'erreur, millisec < 0, OK si message d'erreur");
		Duree d3 = new Duree (-2000);
		Duree d4 = new Duree(d3);
	}
	
	private static void testCompareA(){
		System.out.println("Test cas normaux");
		Duree d1 = new Duree(1);
		Duree d2 = new Duree(2);
		
		int compare1 = d1.compareA(d2); // doit retourner -1
		int compare2 = d1.compareA(d1); // doit retourner 0
		int compare3 = d2.compareA(d1); // doit retourner 1
		
		if (compare1 == -1 && compare2 == 0 && compare3 == 1){
			System.out.println("Test OK");
		}
		
		System.out.println("Test cas d'erreur, OK si message d'erreur");
		Duree d3 = new Duree(-2);
		int compare4 = d1.compareA(d3);
	}
	
	private static void testAjoute(){
		System.out.println("Test cas normaux");
		Duree d1 = new Duree(2);
		Duree d2 = new Duree(5);
		
		d1.ajoute(d2); // d1.getLetemps() doit être = 7
		
		if (d1.getLeTemps()== 7){
			System.out.println("Test OK");
		}
		
		System.out.println("Test cas d'erreur, OK si message d'erreur");
		Duree d3 = new Duree (2);
		Duree d4 = new Duree(-1);
		
		d3.ajoute(d4);
	}
	
	/*private static void testEnJHMS(){
		System.out.println("Test cas normaux");
		// passage de la fonction en public le temps des tests
		Duree d1 = new Duree(95470842); // = 1 jour 2 h 31 min 10 sec 842 ms
		
		int temps[] = d1.enJHMS();
		int jour = temps[0];
		int heure = temps[1];
		int minute = temps[2];
		int sec = temps[3];
		int millisec = temps[4];
		
		if (jour == 1){
			System.out.println("Test jour OK");
		}
		if (heure == 2){
			System.out.println("Test heure OK");
		}
		if (minute == 31){
			System.out.println("Test minute OK");
		}
		if (sec == 10){
			System.out.println("Test seconde OK");
		}
		if (millisec == 842){
			System.out.println("Test millisec OK");
		}
		
	} */
	
	private static void testEnTexte(){
		System.out.println("Test cas normaux");
		Duree d1 = new Duree(95470842); // = 1 jour 2 h 31 min 10 sec 842 ms
		
		String texteJ = d1.enTexte('J');
		//System.out.println(texteJ);
		if (texteJ.equals("1 jour(s) 2 h")){
			System.out.println("Test jours  OK");	
		}
		
		String texteH = d1.enTexte('H');
		//System.out.println(texteH);
		if (texteH.equals("26:31:10")){
			System.out.println("Test heures OK");	
		}
		
		String texteS = d1.enTexte('S');
		//System.out.println(texteS);
		if (texteS.equals("95470.842 sec")){
			System.out.println("Test secondes OK");
		}
			
		String texteM = d1.enTexte('M');
		//System.out.println(texteM);
		if (texteM.equals("95470842 millisec")){
			System.out.println("Test millisecondes OK");	
		}
		
	}
}
