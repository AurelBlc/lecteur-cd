package datas;


class CDTest{

    public static void main (String[] args){

        //Test du constructeur CD(String interpreteCD, String titreCD)
        System.out.println("====Test constructeur CD(String interpreteCD, String titreCD)====");
		testConstruct();

        //Test de la méthode graverCD()
        System.out.println("====Test de la méthode graverCD())====");
        testGraverCD();
        
        //Test de la méthode getDureeTotale()
        System.out.println("====Test de la méthode graverCD())====");
        testGetDureeTotale();
        
        //Test de la méthode graverCD(leFich)
        System.out.println("====Test de la méthode graverCD(leFich))====");
        testGraverCDFichier();
    
    }

    public static void testConstruct(){
        System.out.println("Test cas normal");
        CD cd = new CD("Metallica", "Black Album");

        if((cd.getLInterpreteCD()=="Metallica") && (cd.getLeTitre()=="Black Album")){
            System.out.println("Test OK");
        }
        else{
            System.out.println("Erreur ! Echec du test !");
        }
    }

    public static void testGraverCD(){
        System.out.println("Test cas normal");
        CD cd = new CD("Compile","Compile");
        String titre1 = cd.getUnePlage(1).getLeTitre();
        String interprete1 = cd.getUnePlage(1).getLInterprete();
        long duree1 = cd.getUnePlage(1).getLaDuree().getLeTemps();
        Duree d1 = new Duree(180000);
        long temps1 = d1.getLeTemps();
        
        if((titre1=="TNT") && (interprete1=="AC~DC") && (duree1==temps1)){
            System.out.println("Test 1 OK");
        }
        else{
            System.out.println("Erreur ! Echec du test 1 !");
        }

        String titre2 = cd.getUnePlage(2).getLeTitre();
        String interprete2 = cd.getUnePlage(2).getLInterprete();
        long duree2 = cd.getUnePlage(2).getLaDuree().getLeTemps();
        Duree d2 = new Duree(360000);
        long temps2 = d2.getLeTemps();

        
        if((titre2=="Don't Speak") && (interprete2=="No Doubt") && (duree2==temps2)){
            System.out.println("Test 2 OK");
        }
        else{
            System.out.println("Erreur ! Echec du test 2 !");
        }
    }

    public static void testGetDureeTotale(){
        System.out.println("Test cas normal");

        CD cd = new CD("Compile","Plusieurs Artistes");
        Duree d1 = cd.getDureeTotale();
        long t1 = d1.getLeTemps();

        if(t1==1310500){
            System.out.println("Test getDureeTotale() OK");
        }
        else{
            System.out.println("Erreur ! Echec du test !");
        }
    }

    public static void testGraverCDFichier(){

        CD cd = new CD("ACDC.txt");

        if((cd.getLeTitre().equals("Back in Black")) && (cd.getLInterpreteCD().equals("ACDC"))){
            System.out.println("Test graver(leFich) OK");
        }
        else{
            System.out.println("Erreur ! Echec du test !");
        }
       
    }
}
